//Script writen by Clément SCHMOUKER
//clement dot schmouker at gmail dot com
//29/05/2015
//You are free to use this script as you want, but please, credit me :)
//This document is commented for an easier use !
var words = 'Zaiste, okolica była malownicza! Dwa stawy pochyliły ku sobie oblicza Jako para kochanków: prawy staw miał wody Gładkie i czyste 􏰀ako dziewicze 􏰀agody; Lewy, ciemnie􏰀szy nieco, 􏰀ako twarz młodziana Smagława i 􏰀uż męskim puchem osypana. Prawy złocistym piaskiem połyskał się wkoło, Jak gdyby włosem 􏰀asnym; a lewego czoło Na􏰀eżone łozami, wierzbami czubate; Oba stawy ubrane w zieloności szatę. Z nich dwa strugi468, 􏰀ak ręce związane pospołu, Ściska􏰀ą się. Strug dale􏰀 upada do dołu; Upada, lecz nie ginie, bo w rowu ciemnotę Unosi na swych falach księżyca pozłotę; Woda warstami spada, a na każde􏰀 warście Połyska􏰀ą się blasku miesięcznego garście, Światło w rowie na drobne drzazgi się roztrąca, Chwyta 􏰀e i w głąb niesie toń ucieka􏰀ąca, A z góry znów garściami spada blask miesiąca. Myślałbyś, że u stawu siedzi Świtezianka, Jedną ręką zdró􏰀 le􏰀e z bezdennego dzbanka, A drugą ręką w wodę dla zabawki miota Brane z fartuszka garście zaklętego złota. Dale􏰀, z rowu wybiegłszy, strumień na równinie Rozkręca się, ucisza, lecz widać że płynie, Bo na 􏰀ego ruchome􏰀, drga􏰀ące􏰀 powłoce Wzdłuż miesięczne światełko drga􏰀ące migoce. Jako piękny wąż żmudzki, zwany 􏰏iwojtose􏰁, Chociaż zda􏰀e się drzemać, leżąc między wrzosem, Pełźnie, bo na przemiany srebrzy się i złoci, Aż nagle zniknie z oczu we mchu lub paproci: Tak strumień kręcący się chował się w olszynach, Które na widnokręgu czerniały kończynach, Wznosząc swe kształty lekkie, niewyraźne oku, Jak duchy na wpół widne, na poły w obłoku.',
    wordArray = words.split(' ');

console.log(wordArray);

var nameArray = wordArray; //One of those will be randomly chosen

var lastIndex = 0; //Used to remember the last word picked

var domHeaderName = document.getElementsByClassName("nameContainer")[0]; //Put the ID of the DOM element you want to change here

var bWhichContainer = 0;

//This function will get a random INT used later as the array index
function getRandomInt(iMin, iMax) {
    var newIndex = Math.floor(Math.random() * (iMax - iMin)) + iMin;
    while( lastIndex == newIndex ){
        var newIndex = Math.floor(Math.random() * (iMax - iMin)) + iMin;
    }
  lastIndex = newIndex;  

  return newIndex;
}

function switchContainer( domContainer, index ) {
    
    
    document.getElementsByClassName("nameContainer")[bWhichContainer].classList.add("hidden");
    
    if( bWhichContainer == 0){
        bWhichContainer = 1;
    }
    else {
        bWhichContainer = 0;
    }
  //Change the class name here to fit yours
    document.getElementsByClassName("nameContainer")[bWhichContainer].classList.remove("hidden");
    document.getElementsByClassName("nameContainer")[bWhichContainer].innerHTML = nameArray[index];
}

function pickRandomName() {
    var randomIndex = getRandomInt(0, nameArray.length);

    //Put the randomly chosen name in the DOM
    switchContainer(domHeaderName, randomIndex);
    //loop
    setTimeout(pickRandomName, 2000); //Loop every 2000ms (2s)
}

pickRandomName();